#! /bin/bash
# This contains functions for outputting log messages

ll_error_count=0

# Usual log output
log()
{
	echo -e -n "${@}\n"
}

# Output with appearing "ok" in the end (for processes)
log_ok()
{
	echo -e -n "${@}: ..."
	sleep 0.5
	echo -e "\r${@}: - ok"
}

# Log message in the middle of a line
# Line is made of "-", or of "=" if $1="-i"
log_line()
{
	symbol="-"
	if [[ $1 = "-i" ]]
	then
		symbol="="
		shift
	fi
	if [[ $1 = "-s" ]]
	then
		symbol="$2"
		shift
		shift
	fi
	length=$(expr length "${@}")
	dash_number=$((40 - length/2))
	dashes="$symbol"
	length=$((length % 2))
	i=$((1 + $length))
	while [[ $i != $dash_number ]]
	do
		dashes="$dashes$symbol"
		i=$((i + 1))
	done
	if [[ $length = 0 ]]
	then
		echo -e -n "\n$dashes${@}$dashes\n"
	else
		echo -e -n "\n$dashes${@}$dashes$symbol\n"
	fi
}

# Log message starting with [ERROR], adding 1 to $ll_error_count
log_error()
{
	echo -e -n "[ERROR] ${@}\n"
	ll_error_count=$(($ll_error_count + 1))
}

# Checks whether $1 = $2, outputs error message $3 if not
# $3 must not contain spaces
check()
{
	if [[ $1 != $2 ]]
	then
		shift
		shift
		log_error ${@}
		exit
	fi
}

ll_conclude()
{
	log_line -i "Results:"
	if [[ ll_error_count = 0 ]]
	then
		log "Success"
		return 0
	fi
	log "Failed: $ll_error_count errors detecred"
}