my_cols() { echo -e "cols" | tput -S; }
my_rows() { echo -e "lines" | tput -S; }
my_line() { n=$(my_cols); for ((i=0; i<$n; i++)); do echo -n $1; done; echo; }
alias nl='my_line ='
alias st='git status'
alias gadd='git add'
alias lg='git log'
alias log='lg --oneline'
alias lgs='log -n 5'
s() { nl; lg -n 1; if [[ $? == 0 ]]; then nl; lgs; nl; st; fi; nl; }

