bal() { vim ~/.bash_aliases; bash; }

my_cols() { echo -e "cols" | tput -S; }
my_rows() { echo -e "lines" | tput -S; }
c() { for (( i = $(my_rows); i > 0; i--)) do echo; done; }
alias h='history'; alias hg='history | grep ${@}'

alias la='ls -aF'
alias l='ls -lFshL --full-time --indicator-style=classify'
alias ll='ls -alFshL --full-time --indicator-style=classify'
alias lc='c && ll'

my_cd() { pr_d=$(pwd); cd "${@}"; ls; }; alias cd='my_cd'; alias b='cd $pr_d';
alias back='b'

if [[ -f "$HOME/.bash_git_aliases" ]]; then . "$HOME/.bash_git_aliases"; fi
PS1='\${debian_chroot:+(\$debian_chroot)}\\[\\033[01;34m\\]\\w\\[\\033[00m\\] > '
