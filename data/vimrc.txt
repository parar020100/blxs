set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set mouse=a
set smartindent
set number
set colorcolumn=80
