#! /bin/bash

################################################################################
# Global variables:

this=$(readlink -e "$0")
here=$(dirname $this)
stmp="$here/setup_tmp"
optionfolders=("1_default_begin" "2_custom_begin" "3_default_mid"
			   "4_custom_mid" "5_default_end" "6_custom_end")
instant_run=false
all_custom_options=false
write_stdout=true
write_log=true
cl="
################################################################################
#"

################################################################################

. $here/lib/log_lib

################################################################################
# Functions:

# Checks default options which require special actions
check_default_options()
{
	if [[ $1 = "--all" || $1 = "-a" ]]
	then
		all_custom_options=true
	fi
	if [[ $1 = "--run-now" || $1 = "-r" ]]
	then
		instant_run=true
	fi
	if [[ $1 = "--log" || $1 = "-l" ]]
	then
		write_log=true
	fi
	if [[ $1 = "--silent" || $1 = "-s" ]]
	then
		write_stdout=false
	fi
	if [[ $1 = "--help" || $1 = "-h" ]]
	then
		instant_run=true
	fi
	if [[ $1 = "--default" || $1 = "-d" ]]
	then
		echo "Enabled running with default options. Restarting..."
		$this $(cat $here/data/default_options.txt)
		exit
	fi
	if [[ $1 = "--options-file" || $1 = "-o" ]]
	then
		if [[ ! -f $2 ]]
		then
			echo "[ERROR] File $2 does not exist"
			exit
		fi
		$this $(cat $2)
		exit
	fi
}


# Checks whether option $1 can be found in folder options/$2/;
# if yes, outputs it into file /setup_tmp/$2;
# $3 is 1 if the option has been found before or 0 if not
check_option()
{
	if [[ $# < 3 ]]; then
		echo "Script error"
		exit
	fi
	if [[ -f "$here/options/$2/$1" ]]; then
		if [[ $3 = 0 ]]
		then
			echo "  - option \"$1\" detected"
		fi
		echo -e "\n# Option: $1" >>$stmp/$2
		cat $here/options/$2/$1 >>$stmp/$2
		echo -e "\n# End of option $1" >>$stmp/$2
		return 1
	fi
	return $3
}

# Outputs all files from $1/ into file $2
# starting with message $3 and ending with $4 if $3 and $4 aren't empty
# if $5 exists, will output filenames into stdout
print_folder_files()
{
	if [[ $# < 4 ]]; then
		echo "Script error"
		exit
	fi
	for file_in_folder in $1/*
	do
		test -L $file_in_folder
		if [[ $? = 0 ]]; then
			continue
		fi
		filename=$(basename $file_in_folder)
		if [[ $3 != "" ]]
		then
			echo -e -n "\n$3 $filename:\n" >>$2
		fi
		if [[ $5 ]]
		then
			echo "$5$filename$6"
		fi
		cat $file_in_folder >>$2
		if [[ $4 != "" ]]
		then
			echo -e -n "\n$4 $filename" >>$2
		fi
	done
}

# Printing all options from folder /options/$1 into /setup_tmp/$1
print_all_options()
{
	print_folder_files $here/options/$1 $stmp/$1 "# Option" "# End of option" \
			"  - option \"" "\" added automatically"
}


################################################################################
################################################################################
# Creating temporary folders and files
if [ -d $stmp ]; then
	rm $stmp/*
else
	mkdir $stmp
fi
for file_name in ${optionfolders[@]}; do
	touch $stmp/$file_name
done

# Empty run
if [[ $# == 0 ]]; then
	echo "No arguments. Type $0 -h for help"
	exit
fi
# Looking for parameters
echo "Fetching parameters"
while [[ $# != 0 ]]
do
	check_default_options $1 $2
	cd .
	for file_name in ${optionfolders[@]}; do
		check_option $1 $file_name $?
	done
	if [[ $? = 0 ]]; then
		echo "  [Error] invalid option \"$1\""
	fi
	shift
done
echo "No other options detected"

if $all_custom_options
then
	for folder_name in ${optionfolders[@]}; do
		if [[ $folder_name == *_custom_* ]]
		then
			print_all_options $folder_name
		fi
	done
fi

# Writing into the final file
cp $here/options/setup_template $here/setup
print_folder_files $stmp $here/setup "$cl Section" ""

# Checking for --run-now option and launching
if ! $instant_run; then
	echo -n "Press [enter] to continue with selected options"
	echo " or type anything else to cancell"
	read answer
else
	answer=""
fi
if [[ $answer != "" ]]
then
	echo "Exiting. Run ./setup to execute chosen options"
	exit
fi
if $write_log
then
	if [[ ! -d $here/log ]]
	then
		mkdir $here/log
	fi
	log_filename="$here/log/setup_log_$(date +%Y-%m-%d+%H:%M:%S).txt"
	if $write_stdout
	then
		$here/setup | tee $log_filename
	else
		log_line -i "Running setup in silent mode"
		echo "Log saved in $log_filename"
		$here/setup >$log_filename
	fi
else
	if ! $write_stdout
	then
		$here/setup | log_line -i "Running setup in silent mode"
		exit
	fi
	$here/setup
fi
